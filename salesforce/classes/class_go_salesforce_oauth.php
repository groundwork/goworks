<?php
/**
 * GO Salesforce OAuth
 *
 * The OAuth class provides authentication services to Salesforce.com 
 * Configuration of this class is performed via the GO Salesforce Admin Menus plugin
 *
 * @class 		GO_Salesforce_OAuth
 * @version		0.1
 * @package		GO Salesforce/Classes
 * @author 		Groundwork Opportunities
 */

class GO_Salesforce_OAuth {
	
	private $id;
	private $issued_at;
	private $local_issued_at;
	private $instance_url;
	private $signature;
	private $access_token;
	public $error;
	public $error_desc;
	
	/**
	 * Constructor for the OAuth class. Sets all values to defaults.
	 *
	 * @access public
	 * @return void
	 */
	function __construct() {
		$this->init_vals();
	}
	
	/**
	 * Sets up default values
	 *
	 * @access private
	 * @return void
	 */
	private function  init_vals() {
		$this->id = NULL;
		$this->issued_at = NULL;
		$this->local_issued_at = NULL;
		$this->instance_url = NULL;
		$this->signature = NULL;
		$this->access_token = NULL;
		$this->error = NULL;
		$this->error_desc = NULL;
	}
	
	/**
	 * Clears all existing keys and cache
	 *
	 * @access public
	 * @return void
	 */
	public function clear_keys() {
		$this->init_vals();
		
		update_option('go_salesforce_oauth_id', NULL);
		update_option('go_salesforce_oauth_issued_at', NULL);
		update_option('go_salesforce_oauth_local_issued_at', NULL);
		update_option('go_salesforce_oauth_instance_url', NULL);
		update_option('go_salesforce_oauth_signature', NULL);
		update_option('go_salesforce_oauth_access_token', NULL);
	}
	
	/**
	 * Logins to the Salesforce.com application and returns the id
	 *
	 * @access public
	 * @return id
	 */
	public function get_id() {
		if($id == NULL) {
			$this->check_login();	
		}
		
		return $this->id;
	}
	
	/**
	 * Logins to the Salesforce.com application and returns the issued_at from Salesforce
	 *
	 * @access public
	 * @return issued_at
	 */
	public function get_issued_at() {
		if($issued_at == NULL) {
			$this->check_login();	
		}
		
		return $this->issued_at;
	}
	
	/**
	 * Logins to the Salesforce.com application and returns the issued_at from local time
	 *
	 * @access public
	 * @return local_issued_at
	 */
	public function get_local_issued_at() {
		if($issued_at == NULL) {
			$this->check_login();	
		}
		
		return $this->local_issued_at;
	}
	
	/**
	 * Logins to the Salesforce.com application and returns the instance_url
	 *
	 * @access public
	 * @return instance_url
	 */
	public function get_instance_url() {
		if($instance_url == NULL) {
			$this->check_login();	
		}
		
		return $instance_url;
	}
	
	/**
	 * Logins to the Salesforce.com application and returns the signature
	 *
	 * @access public
	 * @return signature
	 */
	public function get_signature() {
		if($signature == NULL) {
			$this->check_login();	
		}
		
		return $this->signature;
	}
	
	/**
	 * Logins to the Salesforce.com application and returns the access_token
	 *
	 * @access public
	 * @return access_token
	 */
	public function get_token() {
		if($access_token == NULL) {
			$this->check_login();	
		}
		
		return $this->access_token;
	}
	
	/**
	 * Checks cached values and timeout before making call to OAuth provider
	 *
	 * @access private
	 * @return access_token
	 */
	private function check_login() {
		//Retreive all cached values
		$p_id = get_option('go_salesforce_oauth_id');
		$p_issued_at = get_option('go_salesforce_oauth_issued_at');
		$p_local_issued_at = get_option('go_salesforce_oauth_local_issued_at');
		$p_instance_url = get_option('go_salesforce_oauth_instance_url');
		$p_signature = get_option('go_salesforce_oauth_signature');
		$p_access_token = get_option('go_salesforce_oauth_access_token');
		$timeout = get_option('go_salesforce_oauth_token_timeout');

		//Calculate when the cache is set to expire
		$issued_time = intval($p_local_issued_at);
		$expire_time = strtotime("+" . $timeout . " minutes", $issued_time);
		
		//Attempt to login again if cache has expired or if any values are missing in the cache
		if(time() >= $expire_time
			or $p_id == NULL 
			or $p_issued_at == NULL 
			or $p_local_issued_at == NULL 
			or $p_instance_url == NULL 
			or $p_signature == NULL 
			or $p_access_token == NULL) 
		{
			$this->login();
		} else {
			$this->id = $p_id;
			$this->issued_at = $p_issued_at;
			$this->local_issued_at = $p_local_issued_at;
			$this->instance_url = $p_instance_url;
			$this->signature = $p_signature;
			$this->access_token = $p_access_token;
		}
	}
	
	

	/**
	 * Logins to the Salesforce.com application and returns the access_token
	 *
	 * @access private
	 * @return void
	 */
	private function login() {
		$this->clear_keys();
		
		$url = get_option('go_salesforce_oauth_login_uri');
		$client_id = get_option('go_salesforce_oauth_client_id');
		$client_secret = get_option('go_salesforce_oauth_client_secret');
		$username = get_option('go_salesforce_oauth_username');
		$password = get_option('go_salesforce_oauth_password');
		$grant_type = "password";
		
		$fields = array(
		    "client_id" => $client_id,
		    "client_secret" => $client_secret,
		    "username" => $username,
		    "password" => $password,
		    "grant_type" => $grant_type
		);
		
		$fields_string = "";
		//url-ify the data for the POST
		foreach($fields as $key => $value) {
		        $fields_string .= $key . "=" . $value . "&";
		}
		$fields_string = rtrim($fields_string, "&");
		
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		//execute post
		$result = curl_exec($ch);
		
		//close connection
		curl_close($ch);
		
		//decodes the JSON string and get the access_token
		$result = json_decode($result);
		
		$this->id = $result->{'id'};
		$this->issued_at = $result->{'issued_at'};
		$this->local_issued_at = strval(time());
		$this->instance_url = $result->{'instance_url'};
		$this->signature = $result->{'signature'};
		$this->access_token = $result->{'access_token'};
		$this->error = $result->{'error'};
		$this->error_desc = $result->{'error_description'};
		
		//Persist as a cache to reduce unnecessary calls to Salesforce
		update_option('go_salesforce_oauth_id', $this->id);
		update_option('go_salesforce_oauth_issued_at', $this->issued_at);
		update_option('go_salesforce_oauth_local_issued_at', $this->local_issued_at);
		update_option('go_salesforce_oauth_instance_url', $this->instance_url);
		update_option('go_salesforce_oauth_signature', $this->signature);
		update_option('go_salesforce_oauth_access_token', $this->access_token);
	}
}

?>