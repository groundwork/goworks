<?php
/**
 * GO Salesforce Campaign Information
 *
 * The Campaign Information class provides details about a campaign from Salesforce.com 
 * Configuration of this class is performed via the GO Salesforce Admin Menus plugin
 *
 * @class 		GO_Salesforce_Campaign_Info
 * @version		0.2
 * @package		GO Salesforce/Classes
 * @author 		Groundwork Opportunities
 */
class GO_Salesforce_Data {

	private $data;
	private $campaign_ids;
	private $emails;
	private $oauth;
	private $resthelper;
	private $salesforce_error;

	/**
	 * Constructor for the Campaign Info class. Sets all values to defaults.
	 *
	 * @access public
	 * @return void
	 */
	function __construct() {
		$this->init_vals();
	}
	
	/**
	 * Sets up default values
	 *
	 * @access private
	 * @return void
	 */
	private function  init_vals() {

		$this->oauth = new GO_Salesforce_OAuth();
		$this->resthelper = new GO_Salesforce_RestHelper();

		$this->campaigns = array();
		$this->contacts = array();

		$this->salesforce_error = false;
	}
	
	/**
	 * Gets the value of a campaign field
	 *
	 * @access public
	 * @return field value
	 */
	public function campaign_field($campaign_id, $field_name) {
		$this->get_campaign($campaign_id);
		if (!$this->campaigns[$campaign_id]) {
			return null;
		}
		return $this->campaigns[$campaign_id][$field_name];
	}


	/**
	 * Load Leader/Champion data from an email
	 * @access public
	 * @return none
	*/
	public function get_contact($email) {
		$email = strtolower($email);
		if (!$this->contacts[$email]) {
			$res = go_apc_fetch('contact', $email);
			if (!$res) {
				$res = $this->load_data(null, array($email));
				go_apc_store('contact', $email, $res);
			}
			$this->contacts[$email] = $res;
		}
		return $this->contacts[$email];
	}

	/**
	 * Load Campaign data from an ID
	 * @access public
	 * @return none
	*/
	public function get_campaign($campaign_id) {
		if (!$this->campaigns[$campaign_id]) {
			$res = go_apc_fetch('campaign', $campaign_id);
			if (!$res) {
				$data = $this->load_data(array($campaign_id), null);
				if ($data['projects'][0]) {
					$res = $data['projects'][0];
				} else {
					$res = $data['campaigns'][0];
				}
				go_apc_store('campaign', $campaign_id, $res);
			}
			$this->campaigns[$campaign_id] = $res;
		}
		return $this->campaigns[$campaign_id];
	}

	/**
	 * Load campaign information from Salesforce.com
	 *
	 * @access private
	 * @return none
	 */
	private function load_data($campaign_ids, $emails) {
		global $bp;  //Use BuddyPress
		global $salesforce_resthelper;  //Use REST Helper

		// Silence future attempts at salefsorce calls after first error
		if ($this->salesforce_error) {
			$res['campaigns'] = array();
			$res['projects'] = array();
			return $res;
		}
		
		$campaign_ids = $campaign_ids ? $campaign_ids : array();
		$emails = $emails ? $emails : array();
		
		//Get REST detaiils from admin configuration
		$restlet_uri = get_option('go_salesforce_data_restlet_uri');
		
		// Default to 3 hours. Cache is invalidated from go-salesforce-checkout.php
		// whenever a user donates to a campaign. This should ensure immediate fresh data.
		$cache_timeout = 3 * 60 * 60;

		// Fetch items from cache, and update our new list of ids to fetch
		list($campaign_ids, $emails) = $this->load_cache($campaign_ids, $emails);

		if (count($campaign_ids) == 0 && count($emails) == 0) {
			return;
		}

		try {

			//Authenticate with Salesforce
			$access_token = $this->oauth->get_token();

			//rest_helper function is defined in the theme functions.php file
			//Makes webservice call to Salesforce.com
			$res = go_get_object_vars_r(
				$this->resthelper->send_request(
					$restlet_uri
					,json_encode(array(
						'campaignIds' => $campaign_ids,
						'emails' => $emails
					)),'POST'
					,'json'
					,$access_token
				)
			);

			if ($res[0] && $res[0]['errorCode']) {
				throw new Exception("Saleforce error: " . $res[0]['message']);
			}

			// Calculated values
			foreach ($res['projects'] as $key=>$value) {
				$res['projects'][$key]['progress'] = preg_replace( "/[^a-z0-9]/i", "", $res['projects'][$key]['amountRaised']) / preg_replace( "/[^a-z0-9]/i", "", $res['projects'][$key]['goal']) * 100;
			}
			foreach ($res['campaigns'] as $key=>$value) {
				$res['campaigns'][$key]['progress'] = preg_replace( "/[^a-z0-9]/i", "", $res['campaigns'][$key]['amountRaised']) / preg_replace( "/[^a-z0-9]/i", "", $res['campaigns'][$key]['goal']) * 100;
			}

			// Sort campaigns by date so we can easily pull the most recent and then historical
			usort($res['campaigns'], array($this, 'sortCampaignCallback'));

		} catch (Exception $e) {
			xlog($e);
			//An error occurred in the webservice call
			//echo "<pre>";
			//echo var_dump($e);
			//echo "</pre>";
			$res['campaigns'] = array();
			$res['projects'] = array();
			$this->salesforce_error = true;    // Silence future errors/attempts
 		}

		return $res;
	}

	function sortCampaignCallback($a, $b) {
		$date_a = new DateTime($a['startDate']);
		$date_b = new DateTime($b['startDate']);
		if ($date_a == $date_b) {
			return 0;
		} else if ($date_a < $date_b) {
			return 1;
		} else {
			return -1;
		}
	}


	// Update data from cache, return items to be fetched
	private function load_cache($campaign_ids, $emails) {

		$campaign_ids_rval = array();
		$emails_rval = array();

		foreach($campaign_ids as $id) {
			if(!$this->campaigns[$id]) {  // stored in class from prior call?
				$this->campaigns[$id] = go_apc_fetch('campaign', $id);  // fetch from APC
				if (!$this->campaigns[$id]) {
					$campaign_ids_rval[] = $id;  // needs to be fetched
				}
			}
		}

		foreach($emails as $email) {
			if(!$this->contacts[$email]) {  // stored in class from prior call?
				$this->contacts[$email] = go_apc_fetch('contact', $email);  // fetch from APC
				if (!$this->contacts[$email]) {
					$emails_rval[] = $email;  // needs to be fetched
				}
			}
		}

		return array($campaign_ids_rval, $emails_rval);

	}

}
?>