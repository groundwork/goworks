<?php
/**
 * GO Salesforce REST Helper
 *
 * The RestHelper class provides helper functions for connecting to REST webservices
 *
 * @class 		GO_Salesforce_RestHelper
 * @version		0.1
 * @package		GO Salesforce/Classes
 * @author 		Groundwork Opportunities
 */

class GO_Salesforce_RestHelper {
	
	function send_request($url, $data = null, $verb = 'GET', $format = 'json', $access_token = null)
	{
	  $cparams = array(
	    'http' => array(
	      'method' => $verb,
	      'ignore_errors' => true
	    )
	  );

	  
	  if($access_token != NULL) {
	 	 $cparams['http']['header'] = "Authorization: Bearer " . $access_token . "\r\nContent-type: application/json\r\nAccept: application/".$format;
	  }
	
    if (!is_string($data)) { 
	    $params = http_build_query($data);
    }
	  
	  if ($params != false and $params !== null) {
	    if ($verb == 'POST') {
	      $cparams['http']['content'] = $params;
	    } else {
	      $url .= '?' . $params;
	    }
	  }
	  else {
		$cparams['http']['content'] = $data;
	  }
	  
	  // If you're trying to debug the HTTP post/request, try uncommenting the
	  // next line; it will show you the HTTP post/request as an array
	  //var_dump($cparams);
	
	  $context = stream_context_create($cparams);
	  $fp = fopen($url, 'rb', false, $context);
	  if (!$fp) {
	    $res = false;
	  } else {
	    // If you're trying to troubleshoot problems, try uncommenting the
	    // next two lines; it will show you the HTTP response headers across
	    // all the redirects:
	    // $meta = stream_get_meta_data($fp);
	    // var_dump($meta['wrapper_data']);
		
		$res = stream_get_contents($fp);
	  }
	
	  if ($res === false) {
	    throw new Exception("$verb $url failed: $php_errormsg");
	  }

	  switch ($format) {
	    case 'json':
	      $r = json_decode(stripslashes($res));
	      if ($r === null) {
	        throw new Exception("failed to decode $res as json");
	      }
	      return $r;
	
	    case 'xml':
	      $r = simplexml_load_string($res);
	      if ($r === null) {
	        throw new Exception("failed to decode $res as xml");
	      }
	      return $r;
	  }
	  return $res;
	}
}
