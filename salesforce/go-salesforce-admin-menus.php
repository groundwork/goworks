<?php
/*
Plugin Name:  GO Salesforce Admin Menus foo
Version: 0.1
Plugin URI:  http://www.groundworkopportunities.org/
Description: Provides menus to update GO's Salesforce.com configuration. Configuration options can be found under the title 'GO Salesforce' in the Settings menu.
Author: Michael Klein
Author URI:  mailto:mike@groundworkopportunites.org
License:
 Released under the GPL license
  http://www.gnu.org/copyleft/gpl.html
  Copyright 2012 Groundwork Opportunities (email : mike@groundworkopportunites.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


/*-----------------------------------------------------------------------------------*/
/* Section:  "GO Salesforce Config" Admin Menu Code
/*-----------------------------------------------------------------------------------*/
if ( is_admin() ){ // admin actions
	add_action('admin_menu', 'go_salesforce_config_menu');
	
	add_action('admin_init', 'go_salesforce_oauth_init');
	add_action('admin_init', 'go_salesforce_data_init');
	add_action('admin_init', 'go_salesforce_campaign_progress_init');
	add_action('admin_init', 'go_salesforce_donation_info_init');
	add_action('admin_init', 'go_salesforce_signup_info_init');

 	add_action('admin_post_go_salesforce_apc_clear', 'go_salesforce_apc_clear');
} else {
	// non-admin enqueues, actions, and filters	
}

function go_salesforce_config_menu() {
	add_options_page('GO Salesforce Config', 'GO Salesforce', 'manage_options', 'go_salesforce', 'go_salesforce_options_callback');
}

function go_salesforce_options_callback() {
	if (!current_user_can('manage_options')) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	
	echo '<div class="wrap">';
	echo '<h2>Groundwork Opportunities Salesforce Configuration</h2>';
	echo '<form method="post" action="options.php"> ';
	
	settings_fields( 'go_salesforce_options' );
	do_settings_sections( 'go_salesforce_options' );
	
	submit_button();
	echo '</form>';

	echo '<hr/>';
	echo '<form action='.admin_url('admin-post.php').'>';
	echo '<input type="hidden" name="action" value="go_salesforce_apc_clear">';
	echo 'Clear APC User Cache Entries: ';
	submit_button('Clear Cache');
	echo '</form>';

	echo '</div>';
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Admin Custom Post Actions
/*-----------------------------------------------------------------------------------*/
function go_salesforce_apc_clear() {
  echo 'Cache cleared!';
  apc_clear_cache('user');
}
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Section:  "OAuth Webservice Settings" Section Settings
/*-----------------------------------------------------------------------------------*/
function go_salesforce_oauth_init(){
	register_setting( 'go_salesforce_options', 'go_salesforce_oauth_login_uri');
	register_setting( 'go_salesforce_options', 'go_salesforce_oauth_client_id');
	register_setting( 'go_salesforce_options', 'go_salesforce_oauth_client_secret');
	register_setting( 'go_salesforce_options', 'go_salesforce_oauth_username');
	register_setting( 'go_salesforce_options', 'go_salesforce_oauth_password');
	register_setting( 'go_salesforce_options', 'go_salesforce_oauth_token_timeout');
	
	add_settings_section('oauth', 'OAuth Webservice Settings', 'oauth_text', 'go_salesforce_options');
	
	add_settings_field('login_uri_field', 'Login URI', 'login_uri_string', 'go_salesforce_options', 'oauth');
	add_settings_field('client_id_field', 'Client ID', 'client_id_string', 'go_salesforce_options', 'oauth');
	add_settings_field('client_secret_field', 'Client Secret', 'client_secret_string', 'go_salesforce_options', 'oauth');
	add_settings_field('username_field', 'Salesforce Username', 'username_string', 'go_salesforce_options', 'oauth');
	add_settings_field('password_field', 'Salesforce Password', 'password_string', 'go_salesforce_options', 'oauth');
	add_settings_field('token_timeout_field', 'Cached Access Token Timeout (minutes)', 'token_timeout_string', 'go_salesforce_options', 'oauth');

}



function oauth_text() {
	echo '<p>Enter the following OAuth configuration settings to connect to Salesforce.com</p>';
}

function login_uri_string(){
	echo '<input id="client_id_field" name="go_salesforce_oauth_login_uri" size="40" type="text" value="' . get_option('go_salesforce_oauth_login_uri') . '" />';
}

function client_id_string(){
	echo '<input id="client_id_field" name="go_salesforce_oauth_client_id" size="40" type="text" value="' . get_option('go_salesforce_oauth_client_id') . '" />';
}

function client_secret_string(){
	echo '<input id="client_id_field" name="go_salesforce_oauth_client_secret" size="40" type="password" value="' . get_option('go_salesforce_oauth_client_secret') . '" />';
}

function username_string(){
	echo '<input id="username_field" name="go_salesforce_oauth_username" size="40" type="text" value="' . get_option('go_salesforce_oauth_username') . '" />';
}

function password_string(){
	echo '<input id="password_field" name="go_salesforce_oauth_password" size="40" type="password" value="' . get_option('go_salesforce_oauth_password') . '" />';
}

function token_timeout_string(){
	echo '<input id="token_timeout_field" name="go_salesforce_oauth_token_timeout" size="5" type="text" value="' . get_option('go_salesforce_oauth_token_timeout') . '" />';
}

/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Section:  "Campaign Progress Webservice Settings" Section Settings
/*-----------------------------------------------------------------------------------*/
function go_salesforce_campaign_progress_init(){
	register_setting( 'go_salesforce_options', 'go_salesforce_campaign_progress_restlet_uri');
	register_setting( 'go_salesforce_options', 'go_salesforce_campaign_progress_action');
	
	add_settings_section('campaign_progress', 'Campaign Progress Webservice Settings', 'campaign_progress_text', 'go_salesforce_options');
	
	add_settings_field('restlet_uri', 'Restlet Endpoint URL', 'restlet_uri_string', 'go_salesforce_options', 'campaign_progress');
	add_settings_field('action', 'Webservice Action', 'action_string', 'go_salesforce_options', 'campaign_progress');

}

function campaign_progress_text() {
	echo '<p>Enter the following configuration settings to retreive campaign progress information from Salesforce.com</p>';
}

function restlet_uri_string(){
	echo '<input id="restlet_uri_field" name="go_salesforce_campaign_progress_restlet_uri" size="40" type="text" value="' . get_option('go_salesforce_campaign_progress_restlet_uri') . '" />';
}

function action_string(){
	echo '<input id="action_field" name="go_salesforce_campaign_progress_action" size="40" type="text" value="' . get_option('go_salesforce_campaign_progress_action') . '" />';
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Section:  "Donation Information Webservice Settings" Section Settings
/*-----------------------------------------------------------------------------------*/
function go_salesforce_donation_info_init(){
	register_setting( 'go_salesforce_options', 'go_salesforce_donation_info_restlet_uri');
	
	add_settings_section('donation_info', 'Donation Information Webservice Settings', 'donation_info_text', 'go_salesforce_options');
	
	add_settings_field('donation_info_restlet_uri', 'Restlet Endpoint URL', 'donation_info_restlet_uri_string', 'go_salesforce_options', 'donation_info');

}

function donation_info_text() {
	echo '<p>Enter the following configuration settings to send donation information from Salesforce.com</p>';
}

function donation_info_restlet_uri_string(){
	echo '<input id="donation_info_restlet_uri_field" name="go_salesforce_donation_info_restlet_uri" size="40" type="text" value="' . get_option('go_salesforce_donation_info_restlet_uri') . '" />';
}

/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------------*/
/* Section:  "Campaign Signup Webservice Settings" Section Settings
/*-----------------------------------------------------------------------------------*/
function go_salesforce_signup_info_init(){
	register_setting( 'go_salesforce_options', 'go_salesforce_signup_info_restlet_uri');
	
	add_settings_section('signup_info', 'Campaign Signup Information Webservice Settings', 'signup_info_text', 'go_salesforce_options');
	
	add_settings_field('signup_info_restlet_uri', 'Restlet Endpoint URL', 'signup_info_restlet_uri_string', 'go_salesforce_options', 'signup_info');

}

function signup_info_text() {
	echo '<p>Enter the following configuration settings to send campaign signup information to/from Salesforce.com</p>';
}

function signup_info_restlet_uri_string(){
	echo '<input id="signup_info_restlet_uri_field" name="go_salesforce_signup_info_restlet_uri" size="40" type="text" value="' . get_option('go_salesforce_signup_info_restlet_uri') . '" />';
}

/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Section:  "New SalesForce Data Webservice Settings" Section Settings
/*-----------------------------------------------------------------------------------*/
function go_salesforce_data_init(){
	register_setting( 'go_salesforce_options', 'go_salesforce_data_restlet_uri');
	register_setting( 'go_salesforce_options', 'go_salesforce_data_action');
	
	add_settings_section('campaign_data', 'New Salesforce Data Webservice Settings', 'campaign_data_text', 'go_salesforce_options');
	
	add_settings_field('restlet_uri', 'Restlet Endpoint URL', 'campaign_data_restlet_uri_string', 'go_salesforce_options', 'campaign_data');

}

function campaign_data_text() {
	echo '<p>Enter the following configuration settings to retreive campaign progress information from Salesforce.com</p>';
}

function campaign_data_restlet_uri_string(){
	echo '<input id="restlet_uri_field" name="go_salesforce_data_restlet_uri" size="40" type="text" value="' . get_option('go_salesforce_data_restlet_uri') . '" />';
}

/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

?>
