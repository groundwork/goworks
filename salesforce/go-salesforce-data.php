<?php
/*
Plugin Name:  GO Salesforce Donation Information
Version: 0.1
Plugin URI:  http://www.groundworkopportunities.org/
Description:  Gets information about a Campaign from Salesforce.com
Author: Michael Klein
Author URI:  mailto:mike@groundworkopportunites.org
License:
 Released under the GPL license
  http://www.gnu.org/copyleft/gpl.html
  Copyright 2012 Groundwork Opportunities (email : mike@groundworkopportunites.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


/*-----------------------------------------------------------------------------------*/
/* Load OAuth classes
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_OAuth')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_oauth.php');
}
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Load REST Helper class
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_RestHelper')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_resthelper.php');
}
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Load Campaign Info class
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_Data')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_data.php');
}

global $salesforce_data;

if ($salesforce_data == NULL) {
	$salesforce_data = new GO_Salesforce_Data();
}
/*-----------------------------------------------------------------------------------*/





function go_salesforce_get_campaign($campaign_id) {
	global $salesforce_data;

	return $salesforce_data->get_campaign($campaign_id);

}

function go_salesforce_get_contact($email) {
	global $salesforce_data;

	return $salesforce_data->get_contact($email);
}


/**
 * Provides the value of a field from a Salesforce.com campaign
 *
 * @access public
 * @return Value of the salesforce field
 */
function go_salesforce_campaign_field($campaign_id, $field_name) {
	global $salesforce_data;

	$salesforce_data->get_campaign($campaign_id);
	return $salesforce_data->campaign_field($campaign_id, $field_name);
}

/**
 * Provides the value of a field from a Salesforce.com campaign as a shortcode
 *
 * @access public
 * @return Value of the salesforce field
 */
function go_salesforce_campaign_field_shortcode2($atts) {
	extract( shortcode_atts( array(
		'campaign_id' => null,
		'field_name' => null
	), $atts ) );
	
	$rtnVal = null;
	if($campaign_id != null and $field_name != null) {
		$rtnVal = go_salesforce_campaign_field($campaign_id, $field_name);
	}
	return $rtnVal;
}
add_shortcode('campaign-field', 'go_salesforce_campaign_field_shortcode');


/**
 * Provides a formatted funding status bar from Salesforce.com campaign as a shortcode
 *
 * @access public
 * @return HTML formatted progress bar
 */
function go_salesforce_progress_bar_shortcode($atts) {

	extract( shortcode_atts( array(
		'campaign_id' => null,
		'field_list' => null
	), $atts ) );

	return go_salesforce_progress_bar($campaign_id, $field_list);

}
add_shortcode('campaign-progress-bar', 'go_salesforce_progress_bar_shortcode');


function go_salesforce_progress_bar($campaign_id, $field_list=NULL) {
	$rtnVal = null;

	if($field_list == null) {
		//If field list is not provided, use default
		$fieldArr = explode(",", "goal,amountRaised,daysLeft");
	}
	else {
		$fieldArr = explode(",", $field_list);
	}

	if($campaign_id != null) {
		$fieldHtml = null;

    $completed = go_salesforce_campaign_field($campaign_id, 'daysLeft') == 0;

		foreach($fieldArr as $field) {
			if ($field == 'donateButton') {
        if (!$completed) {
  				// Special case for inserting a donate button
  				ob_start();
  				go_salesforce_display_donate_form($campaign_id);
  				$form = ob_get_clean();
  				$fieldHtml = $fieldHtml . sprintf(
  					'<div id="fundraising-goal" class="progress-field-box" style="height: 40px;">'
	  					.'<span class="small" style="margin-top: -15px; margin-left: 10px;">%s</span>'
	  				.'</div>'
	  				,$form
	  			);
        }
			} else {

				$fieldValue = go_salesforce_campaign_field($campaign_id, $field);
				$fieldLabel = go_salesforce_get_pretty_field_name($field);

				if ($field == 'daysLeft' && $fieldValue == 0) {
					// Display campagin completed instead of 0 days left
					$fieldValue = 'Completed!';
					$fieldLabel = 'thank you!';
				}

				$fieldHtml = $fieldHtml . sprintf(
									'<div id="fundraising-goal" class="progress-field-box">'
										.'<span class="big">%s</span>'
										.'<span class="small">%s</span>'
									.'</div>'
									,$fieldValue
									,$fieldLabel
							);
			}
		}
		
		$rtnVal = sprintf('<div class="champion-stats">'
							.'<div class="progress-bar">'
								.'<div class="progress-bar-loader" style="width:%s%%"></div>'
							.'</div>'
							.'%s'
						.'</div>'
						, go_salesforce_campaign_field($campaign_id, "progress")
						, $fieldHtml
				);
	}
	return $rtnVal;
}



/**
 * Provides the "pretty" name for campaign status fields
 *
 * @access public
 * @return "pretty" field name as a string
 */
function go_salesforce_get_pretty_field_name($fieldShortName) {
	
	$rtnVal = $fieldShortName;
	
	if($fieldShortName == "goal") {
		$rtnVal = "goal";
	} elseif ($fieldShortName == "amountRaised") {
		$rtnVal = "raised";
	} elseif ($fieldShortName == "daysLeft") {
		$rtnVal = "days left";
	} elseif ($fieldShortName == "amountWon") {
		$rtnVal = "raised";
	} 

	return $rtnVal;
}
?>
