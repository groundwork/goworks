<?php
/*
Plugin Name:  GO Salesforce Checkout
Version: 0.2
Plugin URI:  http://www.groundworkopportunities.org/
Description:  Sends WooCommerce payment information to Salesforce.com
Author: Michael Klein
Author URI:  mailto:mike@groundworkopportunites.org
License:
 Released under the GPL license
  http://www.gnu.org/copyleft/gpl.html
  Copyright 2012 Groundwork Opportunities (email : mike@groundworkopportunites.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

/*-----------------------------------------------------------------------------------*/
/* Load OAuth classes
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_OAuth')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_oauth.php');
}

global $salesforce_oauth;

if ($salesforce_oauth == NULL) {
	$salesforce_oauth = new GO_Salesforce_OAuth();
}
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* Load REST Helper class
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_RestHelper')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_resthelper.php');
}

global $salesforce_resthelper;

if ($salesforce_resthelper == NULL) {
	$salesforce_resthelper = new GO_Salesforce_RestHelper();
}
/*-----------------------------------------------------------------------------------*/


/**
 * Upon payment receipt, send details about the purchase to Salesforce.com
 *
 * @access public
 * @return none
 */
function woocommerce_payment_complete_send_salesforce($value, $order_id) {
	//Get global references
	global $salesforce_oauth; //Salesforce OAuth
	global $salesforce_resthelper;  //Use REST Helper

	//Get all the order information from the order object
	$order = new WC_Order($order_id);
	$donation_array = array('DonorFN' => $order->billing_first_name
							,'DonorLN' => $order->billing_last_name
							,'DonorEmail' => $order->billing_email
							,'DonorAddress1' => $order->billing_address_1
							,'DonorAddress2' => $order->billing_address_2
							,'DonorCity' => $order->billing_city
							,'DonorZip' => $order->billing_postcode
							,'DonorCountry' => $order->billing_country
							,'DonorState' => $order->billing_state
							,'DonorPhone' => $order->billing_phone
							,'TransactionDateTime' => gmdate('Y-m-d H:i:s', time()) //In GMT
							//,'TransactionDateTime' => $order->order_date; //Not in GMT
							,'OrderID' => $order_id
							,'Campaigns' => array()
						);
	
	//Iterate through all the items in the order and add them to the Campaign array
	foreach ($order->get_items() as $item) {
		$product_id = $item['product_id'];
		$contribution_amt = floatval($item['line_total']);  //Line total includes tax

		//Get the product object to get more details about the items (SKU & Categories)
		$product_obj = new WC_Product($product_id);
		$product_categories = strip_tags($product_obj->get_categories());
		$campaign_id = $product_obj->sku;

		// Clear the APC cache
		go_apc_delete('campaign', $campaign_id);

		//Add campaign object to parent donation object
		$contribution_array = array('UniqueID' => $campaign_id,
					'DonatedAmount' => $contribution_amt,
					'ProductCategories' => $product_categories);		
		array_push($donation_array['Campaigns'], $contribution_array);
	}
	
	//Add the donation object to the array of donations
	$json_obj = array('Donations' => array());
	array_push($json_obj['Donations'], $donation_array);

	//Format as a JSON object	
	$post_data = json_encode($json_obj);
	
	//Uncomment to debug JSON object being sent to Salesforce
	/*
	echo "<pre>";
	var_dump($post_data);
	echo "</pre>";
	*/
	
	//Authenticate with Salesforce
	$access_token = $salesforce_oauth->get_token();
	
	//Get REST URL endpoint from admin settings
	$rest_url = get_option('go_salesforce_donation_info_restlet_uri');

	try {
		//Makes webservice call to Salesforce.com
		$rtnObj = get_object_vars(
				$salesforce_resthelper->send_request(
					$rest_url
					,$post_data
					,'POST'
					,'json'
					,$access_token
				)
		);
		
	} catch (Exception $e) {
		echo "<pre>";
		var_dump($e);
		echo "</pre>";
	}

	return $value;
	//Donor information from the checkout object
	//$checkout =& $woocommerce->checkout->posted;
	
	//Product and contribution information from the cart (cart is cleared 
	//immediately after code is executed)
	//$cart_contents =& $woocommerce->cart->cart_contents;
}
add_filter('woocommerce_payment_successful_result', 'woocommerce_payment_complete_send_salesforce', 100, 2);


/**
 * Gets the product from a SKU
 *
 * @access public
 * @return WC_Product
 */
function go_salesforce_get_product_from_sku($sku) {
	global $woocommerce;
	global $woocommerce_loop;
	
	//Setup arguments for query to find product post
	$args = array(
		'posts_per_page' 	=> 1,
		'post_type'	=> 'product',
		'post_status' => 'publish',
		'ignore_sticky_posts'	=> 1,
		'no_found_rows' => 1,
  	);
  	
	
	//Lookup by SKU
	$args['meta_query'][] = array(
		'key' => '_sku',
		'value' => $sku,
		'compare' => '='
	);
	
	//Execute query to get product post object
	$product_post = new WP_Query( $args );
	$product_id = $product_post->posts[0]->ID;
	$product = new WC_Product($product_id);
	wp_reset_query();
	
	return $product;
}

/**
 * Displays the form to contribute 
 *
 * @access public
 * @return HTML fragment
 */
function go_salesforce_display_donate_form($sku) {
	global $post;

	$product = go_salesforce_get_product_from_sku($sku);
	$product_id = $product->id;

	$post = get_post($product_id);  // Used by woocommerce-gravityforms-addons

	if ($product_id == NULL) { ?>
		Not currently accepting donations
	<?php
	} else {?>


        <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

        <form class="cart" method="post" enctype='multipart/form-data'>
	        <div>

	        	<div style="float: left;">
                <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

                <?php
                        if ( ! $product->is_sold_individually() )
                                woocommerce_quantity_input( array(
                                        'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
                                        'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
                                ) );
                ?>
                <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
	            </div>
                <div style="float: left;"><button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button></div>

			</div>

            <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
        </form>

        <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>


<?

	}
}

?>