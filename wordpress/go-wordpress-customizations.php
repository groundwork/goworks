<?php
/*
Plugin name: GO WordPress Customizations
Description: General small customizations to GO's WordPress installation. This is used instead of functions.php in the theme folder.
Version: 1.0
License: GPLv2
Author: Michael W. Klein
*/


//Disallow image links on blog
update_option('image_default_link_type','none');

/**
 * Add am menu item for users to login to the site
 * @param string $items
 * @param object $args
 * @return string
 */
function go_nav_menu_login_item ( $items, $args ) {

    if ( is_user_logged_in() ) {
        return $items;
    }

    $items .= '<li class="menu-item"><a href="/wordpress/wp-login.php"><span><i class="icon-user-1"></i></span></a></li>';

    return $items;
}
add_filter( 'wp_nav_menu_items', 'go_nav_menu_login_item', 20, 2 );


?>