
<?php

// Adds a widget for the Email subscription form

class GO_Email_Subscription_Form extends WP_Widget {

  /**
  * Sets up the widgets name etc
  */
  public function __construct() {
    parent::__construct(
      'go_email_subscription_form_widget', // Base ID
      __('GO Email Subscription Form', 'text_domain'), // Name
      array( 'description' => __( 'GO Email Subscription Form', 'text_domain' ), ) // Args
    );
  }

  /**
  * Outputs the content of the widget
  *
  * @param array $args
  * @param array $instance
  */
  public function widget( $args, $instance ) {

    ?>
    <script>
    function validateForm(field){
      if (field.value==null || field.value=="" || field.value=="your@email.com" || field.value.indexOf('@') == -1) {
        alert("You must enter a valid email address.");
        field.focus();
        return false;
      } else {
        document.getElementById('go_email_subscription_form').innerHTML = '<h2>Thank You!</h2>';
        return true;
      }
    }
    </script>

    <h3><?= $instance['title']; ?></h3>
    <div id='go_email_subscription_form'>
      <form id="subscribe" onsubmit="return validateForm(this.email);" action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" target="go_email_subscription_hidden">
        <input type="hidden" name="oid" value="00D80000000cn3W">
        <input type="hidden" name="recordType" value="<?= $instance['recordType']; ?>">
        <input type="hidden" name="retURL" value="https://groundworkopportunities.org/wp-content/uploads/go_black.png">
        <input id="email" maxlength="80" name="email" type="text" value="your@email.com" onfocus="this.value='';">
        <input id="email_submit" class="button" name="submit" value="subscribe" type="submit">
      </form>
    </div>
    <iframe name='go_email_subscription_hidden' style='display: none'></iframe>
    <?

  }

  /**
  * Outputs the options form on admin
  *
  * @param array $instance The widget options
  */
  public function form( $instance ) {
    // outputs the options form on admin
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    } else {
      $title = __( 'New title', 'text_domain' );
    }
    if ( isset( $instance[ 'recordType' ] ) ) {
      $recordType = $instance[ 'recordType' ];
    } else {
      $recordType = __( '00000000', 'text_domain' );
    }
    ?>
    <p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    <label for="<?php echo $this->get_field_id( 'recordType' ); ?>"><?php _e( 'recordType:' ); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'recordType' ); ?>" name="<?php echo $this->get_field_name( 'recordType' ); ?>" type="text" value="<?php echo esc_attr( $recordType ); ?>">
    </p>
    <?php 
  }

  /**
  * Processing widget options on save
  *
  * @param array $new_instance The new options
  * @param array $old_instance The previous options
  */
  public function update( $new_instance, $old_instance ) {
    // processes widget options to be saved
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['recordType'] = ( ! empty( $new_instance['recordType'] ) ) ? strip_tags( $new_instance['recordType'] ) : '';

    return $instance;
  } 
}


add_action( 'widgets_init', function(){
  register_widget( 'GO_Email_Subscription_Form' );
});
