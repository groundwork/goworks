<?php
/*
Plugin name: GO Custom BuddyPress Visibility
Description: Creates custom visibility levels for xprofile fields in BP 1.6+
Version: 1.0
License: GPLv2
Author: Michael W. Klein
*/

/**
 * Load BP functions safely
 */
function bbg_cpbv_loader() {
	include( dirname(__FILE__) . '/classes/go_buddypress_custom_bp_field_visibility.php' );
}
add_action( 'bp_include', 'bbg_cpbv_loader' );


//Add new custom role to keep champions separate from other roles
$role = 'champion';
$display_name = 'GO Champion';
$capabilities = array(
    'read' => true
);
add_role( $role, $display_name, $capabilities );

?>