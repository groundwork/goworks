<?php

class BBG_CPBV {
	function __construct() {
		add_filter( 'bp_xprofile_get_visibility_levels', array( &$this, 'modify_levels' ) );
		add_filter( 'bp_xprofile_get_hidden_fields_for_user', array( &$this, 'define_hidden_fields' ), 10, 3 );
	}

	/**
	 * Here's where we add custom levels, or remove levels we don't want
	 *
	 * @param array $levels
	 * @return array $levels
	 */
	function modify_levels( $levels ) {
		// Remove the 'friends' level, if it exists
		//if ( isset( $levels['friends'] ) ) {
		//	unset( $levels['friends'] );
		//}

		// Add a new 'Admins Only' level
		if ( !isset( $levels['admins-only'] ) ) {
			$levels['admins-only'] = array(
				'id' => 'admins-only',
				'label' => __( 'Admins Only', 'textdomain' )
			);
		}
		
		// Add a new 'Member Only' level
		if ( !isset( $levels['member-only'] ) ) {
			$levels['member-only'] = array(
				'id' => 'member-only',
				'label' => __( 'Member Only', 'textdomain' )
			);
		}

		return $levels;
	}

	/**
	 * Here is where we define the actual levels for custom levels, in this case Admins Only
	 *
	 */
	function define_hidden_fields( $hidden_fields, $displayed_user_id, $current_user_id ) {
		// Get the displayed user's visibility settings
		//$user_visibility_levels = bp_get_user_meta( $displayed_user_id, 'bp_xprofile_visibility_levels', true );
	
		// Get the admin fields
		$admin_fields = bp_xprofile_get_fields_by_visibility_levels($displayed_user_id, array('admins-only'));
		if ( !is_super_admin( $current_user_id ) ) {
			$hidden_fields = array_merge($hidden_fields, $admin_fields);
		}
		
		// Get the member only fields
		$member_fields = bp_xprofile_get_fields_by_visibility_levels($displayed_user_id, array('member-only'));
		if (!is_super_admin($current_user_id) and $displayed_user_id != $current_user_id) {
			$hidden_fields = array_merge($hidden_fields, $member_fields);
		}
		
		return $hidden_fields;
	}


}
new BBG_CPBV;

?>