<?php
/*
Plugin name: GO BuddyPress Donor Details
Description: Creates custom menu in BuddyPress for viewing donor information from Salesforce.com. (Requires GO Salesforce plugin to be enabled)
Version: 0.1
License: GPLv2
Author: Michael W. Klein
*/

bp_go_donors_setup_globals();

/**
 * bp_go_donors_setup_globals()
 *
 * Sets up GO Donor's global variables.
 */
function bp_go_donors_setup_globals() {
	global $bp, $wpdb;
	
	$bp->donors = new stdClass();
	$bp->donors->id = 'donor';
	$bp->donors->slug = 'donors';
}

/**
 * bp_go_donors_setup_nav()
 *
 * Sets up the user profile navigation items for the component. This adds the top level nav
 * item. This is then rendered in the template.
 */
function bp_go_donors_setup_nav() {
    if(bp_go_donors_privacy_level_permitted()>5) {

		global $bp;
		
		$nav_item_name = apply_filters( 'bp_go_donors_nav_item_name', __( 'Donors', 'bp-donors' ) );
	
		bp_core_new_nav_item( array(
			'name' => $nav_item_name,
			'slug' => $bp->donors->slug,
			'position' => 20,
			'screen_function' => 'bp_go_donors_display',
			'default_subnav_slug' => $bp->donors->slug,
			'item_css_id' => $bp->donors->slug . '_link',
			'show_for_displayed_user' => true
		) );
    }
}
add_action( 'bp_setup_nav', 'bp_go_donors_setup_nav' );


/**
 * bp_go_donors_display()
 *
 * Callback from the BP nav menu to handle clicking the donors link. This uses the BP templating
 * to output two pieces of the information: (1) The page title and (2) The donor page content.
 */
function bp_go_donors_display() {
	if(bp_go_donors_privacy_level_permitted()>5) {
		add_action( 'bp_template_title', 'bp_go_donors_screen_title' );
	    add_action( 'bp_template_content', 'bp_go_donors_screen_content' );
		bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ));
	}
}

/**
 * bp_go_donors_screen_title()
 *
 * Outputs the donor page title to the screen
 */
function bp_go_donors_screen_title() {
	echo 'Donor Information';
}

/**
 * bp_go_donors_screen_content()
 *
 * Outputs the donor page content to the screen
 */
function bp_go_donors_screen_content() {
	if(bp_go_donors_privacy_level_permitted()>5) {
	
		global $bp;
		$user_info = get_userdata( $bp->displayed_user->id ); 
		$user_email = $user_info->user_email;
		$contact = go_salesforce_get_contact($user_email); 
		
		foreach($contact['campaigns'] as $campaign) {
		?>
		<div><?= $campaign['projectCampaignId'] ?></div>
		<table>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Donation Date</th>
				<th>Amount</th>
			</tr><?php foreach ( $campaign['donations'] as $donor ) { ?>
			<tr>
				<td><?php echo $donor['donorFirstName']; ?></td>
				<td><?php echo $donor['donorLastName']; ?></td>
				<td><?php echo $donor['donorEmail']; ?></td>
				<td><?php echo $donor['dateOfDonation']; ?></td>
				<td><?php echo $donor['donationAmount']; ?></td>
			</tr><?php } ?>
		</table>
		<?
		}
	}
}


/**
 * bp_go_donors_privacy_level_permitted()
 *
 * Comments
 */
function bp_go_donors_privacy_level_permitted(){
	global $bp;
	
	if(!is_user_logged_in())
		return 0;
	elseif(is_super_admin())
		return 10;
	elseif ( ($bp->displayed_user->id && $bp->displayed_user->id == $bp->loggedin_user->id) )
		return 6;
	elseif ( ($bp->displayed_user->id && function_exists('friends_check_friendship') && friends_check_friendship($bp->displayed_user->id,$bp->loggedin_user->id) ) )
		return 4;
	else
		return 2;
}

?>