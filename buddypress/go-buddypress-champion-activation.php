<?php
/*
Plugin name: GO BuddyPress Champion Activation
Description: Custom changes to the user sign-up to create product and salesforce entries for new champions.
Version: 1.0
License: GPLv2
Author: Brian Shire
*/


add_action( 'bp_core_activated_user', 'go_bp_activated_user' );

// ------ Debug for bot sign-ups -----
//add_action( 'bp_signup_validate', 'go_bp_signup_validate');  // For debugging
//
//function go_bp_signup_validate() {
//  // Log out all our post data so we can debug bot sign-ups
//  $data = var_export($_POST, true);
//  xlog("GO_BP_SIGNUP_VALIDATE: \n".$data);
//}
//

function go_bp_activated_user($user_id, $key=NULL, $user=NULL) {

  // Set user's role to "GO Champion"
  $wp_user = new WP_User($user_id);
  $wp_user->set_role("champion");

	$user = bp_core_get_core_userdata($user_id);

	//Add the donation object to the array of donations
	$json_obj = array('email' 		=> 	$user->user_email,
					  'name' 		=>	xprofile_get_field_data('Name', $user_id),
					  'hometown' 	=>	xprofile_get_field_data('Hometown', $user_id),
					  'state' 		=>	xprofile_get_field_data('State', $user_id),
					  'ideaId' 		=> 	xprofile_get_field_data('Idea ID', $user_id),
					  'ideaURL' 	=> 	xprofile_get_field_data('Idea URL', $user_id),
					  'goal' 		=> 	xprofile_get_field_data('Goal', $user_id),
					  'inspiration' => 	xprofile_get_field_data('Inspiration', $user_id)
					);

  // Remove an common invalid chars ('$', ',') from goal value so it's actualy numeric
  $json_obj['goal'] = intval(str_replace(array('$', ','), array('', ''), $json_obj['goal'])); 
  xprofile_set_field_data('Goal', $user_id, $json_obj['goal']); 


	$salesforce_oauth = new GO_Salesforce_OAuth();
	$salesforce_resthelper = new GO_Salesforce_RestHelper();

	$salesforce_oauth->get_token();

	//Format as a JSON object	
	$post_data = json_encode($json_obj);

	//Authenticate with Salesforce
	$access_token = $salesforce_oauth->get_token();
	
	//Get REST URL endpoint from admin settings
	$rest_url = get_option('go_salesforce_signup_info_restlet_uri');

	try {
		//Makes webservice call to Salesforce.com
		$rtnObj = json_decode($salesforce_resthelper->send_request(
					$rest_url
					,$post_data
					,'POST'
					,'json'
					,$access_token
				));
	} catch (Exception $e) {
		echo "<pre>";
		var_dump($e);
		echo "</pre>";
	}

  $campaignId = $rtnObj->Id;
  $product = array(
           'post_type'     => 'product',
           'post_title'    => 'Go Champion '. xprofile_get_field_data('Name', $user_id),
           'post_content'  => '<iframe class ="video" src="http://player.vimeo.com/video/39670519?title=0&amp;byline=0&amp;portrait=0" frameborder="0" width="800" height="450"></iframe>
  <h3>GO Champions: Give Your 100%</h3>
  <strong>Connecting leaders.</strong> Be the change you want to see in the world by adopting a cause you feel passionate about to make a real impact. We connect you directly to the project and the community that needs your support. GO Champions then get their own personal fundraising page which gives 100% of the fund

  <strong>It\'s time to start believing in charity again.</strong>',
           'post_excerpt'  => "<strong>Connecting leaders.</strong> Be the change you want to see in the world by adopting a cause you feel passionate about to make a real impact. We connect you directly to the project and the community that needs your support. GO Champions then get their own personal fundraisin

  <strong>It's time to start believing in charity again.</strong>",
           'post_short_description'  => "short description 2",
           'post_status'   => 'publish',
           //'post_thumbnail' => 'http://groundworkopportunities.org/wp-content/uploads/2012/10/4-2.png',
           'post_author'   => $user_id
  );

  // Insert the product into the database
  // Post filtering
  remove_filter('content_save_pre', 'wp_filter_post_kses');
  remove_filter('content_filtered_save_pre', 'wp_filter_post_kses');
  $new_product_id = wp_insert_post( $product );

  // Change Product Category
  wp_set_object_terms($new_product_id, 'GO Champion', 'product_cat');


  //Upload image product
  $filename = "http://groundworkopportunities.org/wp-content/uploads/2012/10/4-2.png";
  $wp_filetype = wp_check_filetype(basename($filename), null );
  $attachment = array(
          'post_mime_type' => $wp_filetype['type'],
          'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
          'post_content' => '',
          'post_status' => 'inherit'
  );
  $attach_id = wp_insert_attachment( $attachment, $filename, $new_product_id );
  $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
  wp_update_attachment_metadata( $attach_id, $attach_data );
  set_post_thumbnail( $new_product_id, $attach_id );

  // Set the product's gravity form id to "Campaign Contribute"
  //$gravity_form_data = get_post_meta($new_product_id, '_gravity_form_data', true);
  $gravity_form_data = array();
  foreach (RGFormsModel::get_forms() as $form) {
    if ($form->title == 'Campaign Contribute') {
      $gravity_form_data['id'] = $form->id;
      // Setting price before/after causes gravity forms to not display "Free!", total hack
      $gravity_form_data['price_before'] = '';
      $gravity_form_data['price_after'] = '';
      update_post_meta($new_product_id, '_gravity_form_data', $gravity_form_data);
      break;
    }
  }

  // Set user's campaign ID value (mirror's product SKU)
  xprofile_set_field_data('Campaign ID', $user_id, $campaignId); 

  update_post_meta($new_product_id, '_sku', $campaignId);
  update_post_meta($new_product_id, '_visibility', 'hidden');
  update_post_meta($new_product_id, '_regular_price', '0');
  update_post_meta($new_product_id, '_virtual', 'yes');
  update_post_meta($new_product_id, '_downloadable', 'no');
  update_post_meta($new_product_id, '_coupon_prefix', 'CC');
  update_post_meta($new_product_id, '_product-type', 'simple');
  update_post_meta($new_product_id, '_stock_status', 'instock');
  update_post_meta($new_product_id, '_manage_stock', 'no');
  update_post_meta($new_product_id, '_stock', '0');
  update_post_meta($new_product_id, '_price', '0');
  update_post_meta($new_product_id, '_sale_price', '');
  update_post_meta($new_product_id, '_sale_price_dates_from', '');
  update_post_meta($new_product_id, '_sale_price_dates_to', '');
  update_post_meta($new_product_id, '_tax_status', 'taxable');
  update_post_meta($new_product_id, '_tax_class', '');
  update_post_meta($new_product_id, '_featured', 'no');

  // Post filtering
  add_filter('content_save_pre', 'wp_filter_post_kses');
  add_filter('content_filtered_save_pre', 'wp_filter_post_kses');


}



//Change sign up link to become-a-champion page
function new_login_link(){
  global $bp;
  if ( is_user_logged_in() )
    return false;
  echo '<li class="bp-login no-arrow"><a href="' . bp_get_root_domain() . '/wp-login.php?redirect_to=' . urlencode( bp_get_root_domain() ) . '">' . __( 'Log In', 'buddypress' ) . '</a></li>';
  // Show "Sign Up" link if user registrations are allowed
  if ( bp_get_signup_allowed() )
    echo '<li class="bp-signup no-arrow"><a href="../become-a-champion/">' . __( 'Sign Up', 'buddypress' ) . '</a></li>';
}
remove_action( 'bp_adminbar_menus', 'bp_adminbar_login_menu', 2 );
add_action( 'bp_adminbar_menus', 'new_login_link', 2 );



    
//Warning if you are already a champion
function champions_existing(){
  if(isset($_POST['champion-idea'])) {
    $current_user = wp_get_current_user();
    $current_user_id = $current_user->ID;
    if ($current_user_id == 0 ) return;
    if($data = xprofile_get_field_data( 'Idea', $current_user_id)) {
      bp_core_add_message( __('You are already fundraising for ' . $data . '. You cannot fundraise for another cause until you complete this one.', 'buddypress' ), 'error' );
    } else {
      xprofile_set_field_data('Idea', $current_user_id, $_POST['champion-idea']);
      xprofile_set_field_data('Idea URL', $current_user_id, $_POST['champion-url']);
      xprofile_set_field_data('Idea ID', $current_user_id, $_POST['champion-ideaId']);
      bp_core_add_message( __('Almost there! Just a few more fields to fill in before your page is active. Already have an account? <a href="../wp-login.php?redirect_to=http%3A%2F%2Fwww.groundworkopportunities.org">Login</a> here.', 'buddypress' ) );
    }
  } 
}
add_action('init', 'champions_existing');


?>
