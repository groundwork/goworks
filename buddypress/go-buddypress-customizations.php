<?php
/*
Plugin name: GO BuddyPress Customizations
Description: General small customizations to GO's installation/use of BuddyPress. This is used instead of functions.php in the theme folder.
Version: 1.0
License: GPLv2
Author: Michael W. Klein
*/


// Customize our menu items
function go_bp_nav() {
    //bp_core_remove_nav_item('profile');
    bp_core_remove_nav_item('album');
    bp_core_remove_nav_item('friends');
}
add_action('bp_setup_nav', 'go_bp_nav', 100);

// Set messages from other champions pages										 	
function champions_notifications(){
	if(isset($_POST['champion-message'])) {
		bp_core_add_message( __( stripslashes($_POST['champion-message']), 'buddypress' ) );
		global $woocommerce;	
		$woocommerce->add_message( __( stripslashes($_POST['champion-message']), 'woocommerce') );
	}
}
add_action('init', 'champions_notifications');


// Adding BuddyPress Navigation Item - Wall											 
function my_bp_nav_adder() { 
    bp_core_new_nav_item( 
        array( 
            'name' => __('Wall', 'buddypress'), 
            'slug' => 'wall', 
            'position' => 20, 
            'show_for_displayed_user' => true, 
            'screen_function' => 'wall_link', 
            'item_css_id' => 'wall' 
        )
    ); 
    print_r($wp_filter); 
} 

function wall_link () { 
    //add content and call the members plugin.php template 
    add_action( 'bp_template_content', 'my_groups_page_function_to_show_screen_content' ); 
    bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) ); 
}

function my_groups_page_function_to_show_screen_content() { 
	?><div class="fb-comments" data-href="<?php global $bp; echo $bp->displayed_user->domain; ?>" data-num-posts="10" data-width="940"></div><?php 
} 

add_action( 'bp_setup_nav', 'my_bp_nav_adder' ); 

// ----------------------------------------------------------------------------------
// Handle custom display of groups fields for sign-up, ie: Champion or Leader sign-up
// ----------------------------------------------------------------------------------
function go_bp_signup_profile_fields() {
    load_template(locate_template('buddypress/members/register-custom.php'));
}
add_action('bp_signup_profile_fields', 'go_bp_signup_profile_fields');

// Used by register-custom.php template to determine which group profile fields to display
function go_get_profile_group_id() {
    global $profile_template;

    bp_has_profile();

    foreach($profile_template->groups as $group) {
        if ($group->name == 'Leader') {
            $leader_id = $group->id;
        } else if ($group->name == 'Champion') {
            $champion_id = $group->id;
        }
    }

    if ($_GET['type'] == 'leader') {
        return $leader_id;
    }

    return $champion_id;
}
// ----------------------------------------------------------------------------------

?>