<?php
/*
Plugin Name:  GO Works Plugin
Version: 0.1
Plugin URI:  http://www.groundworkopportunities.org/
Description: Provides GO champion registration, campaign, and donation processes.
Author: Michael Klein, Kyle Miller, Brian Shire
Author URI:  mailto:go-tech-leads@groundworkopportunites.org
License:
 Released under the GPL license
  http://www.gnu.org/copyleft/gpl.html
  Copyright 2012 Groundwork Opportunities (email : mike@groundworkopportunites.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

/* Entry point for plugin, include hooks and includes here! */

// Libraries
include('lib/misc.php');

// Original go-salesforce plugin code
include('salesforce/go-salesforce-admin-menus.php');
//include('salesforce/go-salesforce-campaign-info.php');
include('salesforce/go-salesforce-checkout.php');
include('salesforce/go-salesforce-data.php');

// GO's WordPress customizations
include('wordpress/go-wordpress-customizations.php');

// Original go-buddypress plugin code
include('buddypress/go-buddypress-champion-activation.php');
include('buddypress/go-buddypress-custom-visibility.php');
include('buddypress/go-buddypress-donors.php');
include('buddypress/go-buddypress-customizations.php');

// Woocommerce
include('woocommerce/go-woocommerce-customizations.php');

include('widgets/email-subscription.php');
include('shortcodes/woocommerce.php');
