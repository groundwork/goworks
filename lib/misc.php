<?php
/*
Description: Misc Library functions
License:
 Released under the GPL license
  http://www.gnu.org/copyleft/gpl.html
  Copyright 2012 Groundwork Opportunities (email : mike@groundworkopportunites.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

// Converts a given object to an array, just like PHP's get_object_vars except recursive
function go_get_object_vars_r($object) {
	
	if (is_object($object)) {
		$object = get_object_vars($object);
	}

	if (is_array($object)) {
		foreach ($object as $k=>$v) {
			$object[$k] = go_get_object_vars_r($object[$k]);
		}
	}

	return $object;
}



// APC helper functions, wraps apc functions with additional prefix parameter

$go_log_cache = false;  /* for debugging */

function go_apc_fetch($prefix, $id) {
	$res = null;
	global $go_log_cache;
	if (function_exists('apc_fetch')) {
		$res = apc_fetch($prefix.'_'.$id);
	}
	if ($res && $go_log_cache) {
		xlog('GO Cache HIT: '.$prefix. ' '. $id);
	} else if ($go_log_cache) {
		xlog('GO Cache MISS: '.$prefix. ' '. $id);
	}
	return $res;
}

function go_apc_store($prefix, $id, $value, $timeout=0) {
	global $go_log_cache;
	if (function_exists('apc_store')) {
		apc_store($prefix.'_'.$id, $value, $timeout);
		if ($res && $go_log_cache) {
			xlog('GO Cache STORE: '.$prefix. ' '. $id);
		}
	}
}

function go_apc_delete($prefix, $id) {
	global $go_log_cache;
	if (function_exists('apc_delete')) {
		apc_delete($prefix.'_'.$id);
		if ($res && $go_log_cache) {
			xlog('GO Cache DELETE: '.$prefix. ' '. $id);
		}
	}
}