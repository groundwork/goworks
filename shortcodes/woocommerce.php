<?php

/**
 * Provides the value of a field from a Salesforce.com campaign as a shortcode
 *
 * @access public
 * @return Value of the salesforce field
 */
function go_shortcode_addToCart($atts) {
	global $woocommerce;

  extract( shortcode_atts( array(
		'product_id' => null,
		'show_amount' => 1,
		'button_text' => 'Add to Cart'
	), $atts ) );

	if ($product_id == null) return '';

	$product_data = get_post( $product_id );

		$product = wc_setup_product_data( $product_data );
		$link = $product->add_to_cart_url();

		$rval = '';
		$rval .= '<form action="ideas/'.$link.'"" method="post" enctype=“multipart/form-data">';
		if ($show_amount) {
			$rval .= '<b>AMOUNT ($)</b> <input id="input_1_1" tabindex="1" type="text" name="input_1" value="" size=6/>  <? } ?>';
		}	
		$rval .= '<button type="submit">'.$button_text.'</button></form>';

		return $rval;
}
add_shortcode('go_addToCart', 'go_shortcode_addToCart');

