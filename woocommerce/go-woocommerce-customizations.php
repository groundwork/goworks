<?php

//Change Add to Cart text
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');
function woo_custom_cart_button_text() {
	return __('Donate', 'woocommerce');
}


function go_woocommerce_head() {
    $product = new WC_Product( get_the_ID() );
    $sku = $product->sku;
    $campaign = go_salesforce_get_campaign($sku);
    if ($campaign && $campaign['daysLeft'] == 0) {
      ?>
        <style>
          form.cart {
            display: none;
          }
        </style>
      <?
    }
}
add_action('wp_head', 'go_woocommerce_head');

// Start sessions for tipping functionality
function start_my_session() {
    if(!session_id()) {
        session_start();
    }
}

function end_my_session() {
    session_destroy ();
}
add_action('init', 'start_my_session', 1);
add_action('wp_logout', 'end_my_session');
add_action('wp_login', 'end_my_session');


// Tipping functionality															 
function add_product_to_cart() {
	if ( ! is_admin() ) {
		global $woocommerce;
		$product_id = 246922173;
		$found = false;
		//check if product already in cart
		if ( $woocommerce->cart && sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
			foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
				$_product = $values['data'];
				if ( $_product->id == $product_id or $_product->product_type == 'subscription')
					$found = true;	
			}
			// if product not found, add it
			if ( !$found && $_SESSION['added'] != 1) {
				$woocommerce->cart->add_to_cart( $product_id);
				$_SESSION['added'] = 1;
			}
		}
	}
}
add_action( 'init', 'add_product_to_cart' );

// List progress bar after images
function go_product_progress_bar() { 
	$product = new WC_Product( get_the_ID() );
	$sku = $product->sku;
	$campaign = go_salesforce_get_campaign($sku);
	if (!empty($campaign)) {
		echo go_salesforce_progress_bar($sku); 
		go_champion_signup_btn($product);
	}
}
add_action( 'woocommerce_product_thumbnails', 'go_product_progress_bar', 50 );

// List progress bar on product listings
function go_woocommerce_after_shop_loop_item_title() {
	$product = new WC_Product( get_the_ID() );
	$sku = $product->sku;
	$campaign = go_salesforce_get_campaign($sku);
	if (!empty($campaign)) {
		echo '<div style="font-size: 0.8em;">';
		echo go_salesforce_progress_bar($sku); 
		echo '</div>';
		echo '<div style="padding-top: 50px;"><a href="'.get_the_permalink().'">Click to learn more...</a></div>';
	}
}
add_action('woocommerce_after_shop_loop_item_title', 'go_woocommerce_after_shop_loop_item_title');

function go_champion_signup_btn() {

	if (is_user_logged_in()) {
		$action = bp_get_loggedin_user_link();
	} else {
		//$action = "/register";
		$action = '/go-volunteer-interest-form/';
	}

  $product = new WC_Product(get_the_ID());
  $campaign = go_salesforce_get_campaign($product->sku);
  $complete = $campaign['daysLeft'] == 0;

	?>
        <div class="become-champion">
                <form action="<?= $action ?>" method="post">
                        <input type="hidden" name="champion-idea" value="<?php global $product; echo the_title() /* . ' in ' . strip_tags($product->get_categories()) */; ?>" />
                        <input type="hidden" name="champion-url" value="<?php echo the_permalink(); ?>" />
                        <input type="hidden" name="champion-ideaId" value="<?php global $product; echo $product->sku;?>" />
                        <? if (!$complete) { ?> 
                        <button type="submit" class="go-button">Fundraise for This Idea</button>
                        <? } ?>
                </form>
        </div>
    <?

}

// List champions after progress bar
function go_product_champions() { 
	global $bp;
	$product = new WC_Product( get_the_ID() );
    $campaign_id = $product->sku;
    $categories = explode(",",strip_tags($product->get_categories()));
    $campaign = go_salesforce_get_campaign($campaign_id) ;
    $numColumns = 3;
    $userCount = $numColumns;
?>

<?php if (in_array("GO Project", $categories)) : ?>
	<div id="ideas-champion">
		<h3>
			<span class="meet-champions"><strong>Meet The Champions</strong></span>
		</h3>
		<div class="champions-div">

					<?php foreach ( $campaign['campaigns'] as &$the_campaign ) : ?>
						<?php foreach ( $the_campaign['champions'] as &$champion ) : ?>
							<?php $user = get_user_by( 'email', $champion['email'] );  ?>
							<?php if($userCount % $numColumns == 0) : ?><tr><?php endif; ?>
									<div>
										<a href="<?php echo bp_core_get_user_domain( $user->id ); ?>" title="<?php echo bp_core_get_user_displayname( $user->id ); ?>">
											<?php echo bp_core_fetch_avatar ( array( 'item_id' => $user->id, 'type' => 'full', 'height' => '100', 'width' => '100' ) ) ?>
											<div class="champion-name"><?php echo bp_core_get_user_displayname( $user->id ); ?></div>
										</a>
									</div>
								<?php $userCount = $userCount + 1; ?>
							<?php if($userCount % $numColumns == 0) : ?></tr><?php endif; ?>
						<?php endforeach; ?>
					<?php endforeach; ?>
					<?php if($userCount % $numColumns != 0) : ?></tr><?php endif; ?>

		</div>
	</div>
<?php endif; ?>
<?

}
add_action( 'woocommerce_product_thumbnails', 'go_product_champions', 60 );


// Reverse kleo code and instead make the description tabs full width of page after other content
function go_move_tabs() {
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 31 );
	add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
}
add_action( 'after_setup_theme', 'go_move_tabs', 100);
?>
